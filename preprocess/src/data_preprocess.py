import os
import pandas as pd
from sklearn.model_selection import train_test_split
import tensorflow_data_validation as tfdv


RAW_SCHEMA_DIR = "data/"
SCHEMA_PATH = os.path.join(RAW_SCHEMA_DIR, 'schema.pbtxt')
STATS_PATH = os.path.join(RAW_SCHEMA_DIR, "data_stats.stats")


def data_distribution(data):
   stats = tfdv.generate_statistics_from_dataframe(
       dataframe=data,
       stats_options=tfdv.StatsOptions(
           label_feature='tip_bin',
           weight_feature=None,
           sample_rate=1,
           num_top_values=50
       )
   )
   schema = tfdv.infer_schema(statistics=stats)
   tfdv.write_schema_text(schema, SCHEMA_PATH)
   tfdv.write_stats_text(stats, STATS_PATH)


if __name__ == '__main__':
   df = pd.read_csv("raw.csv")
   data_distribution = data_distribution(data=df)

   X = df.drop(columns=['Response'])
   y = df['Response']
   X_train, X_test, y_train, y_test = train_test_split(
       X, y, test_size=0.33, random_state=42)

   X_train.to_csv("data/train/value/data.csv", index=False)
   X_test.to_csv("data/test/value/data.csv", index=False)
   y_train.to_csv("data/train/label/label.csv", index=False)
   y_test.to_csv("data/test/label/label.csv", index=False)
