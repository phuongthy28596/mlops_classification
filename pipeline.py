import argparse
from train.src.experience import experience

parser = argparse.ArgumentParser()
parser.add_argument(
    '-n', '--n_epoch', required=True, type=int)
parser.add_argument(
    '-train', '--train_folder', required=True, type=str)
parser.add_argument(
    '-test', '--test_folder', required=True, type=str)


if __name__ == '__main__':
    args = parser.parse_args()
    experience(n_epoch=args.n_epoch,
               train_folder=args.train_folder,
               val_folder=args.test_folder)
