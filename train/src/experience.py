import numpy as np
import pandas as pd
import torch

from sklearn import preprocessing
from sklearn_pandas import DataFrameMapper
from sklearn import svm, metrics


CATEGORICAL_FEATURE_KEYS = [
    'Gender',
    'Driving_License',
    'Previously_Insured',
    'Vehicle_Age',
    'Vehicle_Damage'
]
NUMERIC_FEATURE_KEYS = [
    'Age',
    'Region_Code',
    'Annual_Premium',
    'Policy_Sales_Channel',
    'Vintage'
]


def data_transformer(data):
    mapper = DataFrameMapper(
        [([continuous_col], preprocessing.StandardScaler()) for continuous_col in NUMERIC_FEATURE_KEYS] +
        [(categorical_col, preprocessing.LabelEncoder()) for categorical_col in CATEGORICAL_FEATURE_KEYS]
    )
    transformer = mapper.fit(data)
    return transformer

def model_definition():
    return svm.SVC()

def train_epoch(transformer, model):
    model.fit(
        transformer.transform(train_data), train_label)
    return model, metrics.accuracy_score(model.predict(transformer.transform(train_data)), train_label)

def val_epoch(transformer, model):
    y_predict = model.predict(transformer.transform(val_data))
    # Measure the performance
    print("Accuracy score %.3f" % metrics.accuracy_score(val_label, y_predict))
    return metrics.accuracy_score(val_label, y_predict)


def train(n_epoch, transformer, model):
    best_val_loss = np.inf
    model, train_log = train_epoch(transformer=transformer, model=model)
    val_log = val_epoch(transformer=transformer, model=model)
    best_val_loss = val_log
    torch.save(model, 'model/best_model.pth')
    print("Epoch", n_epoch)
    print("Training log:", train_log)
    print("Validation log:", val_log)


def experience(n_epoch, train_folder, val_folder):
    global train_data
    global val_data
    global train_label
    global val_label

    train_data = pd.read_csv(train_folder + "/value/data.csv").head(1000)
    val_data = pd.read_csv(val_folder + "/value/data.csv").head(10)
    train_label = pd.read_csv(train_folder + "/label/label.csv").head(1000)
    val_label = pd.read_csv(val_folder + "/label/label.csv").head(10)

    transformer = data_transformer(data=train_data)
    model = model_definition()
    train(n_epoch=1, transformer=transformer, model=model)
